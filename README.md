# OpenML dataset: COVID-19-Rio-de-Janeiro-(City)

https://www.openml.org/d/43509

## Structure

The dataset has the following file structure:

* `dataset/`
  * `tables/`
    * [`data.pq`](./dataset/tables/data.pq): Parquet file with data
  * [`metadata.json`](./dataset/metadata.json): OpenML description of the dataset
  * [`features.json`](./dataset/features.json): OpenML description of table columns
  * [`qualities.json`](./dataset/qualities.json): OpenML qualities (meta-features)

## Description

Context - World Health Organization (WHO)
Coronavirus disease (COVID-19) is an infectious disease caused by a newly discovered coronavirus (2019-nCoV).

Content
This dataset has information on the number of confirmed cases, deaths, and recoveries (by neighborhood) in the city of Rio de Janeiro, Brazil. 
Please note that this is a time-series data and so the number of cases on any given day is a cumulative number.
The number of new cases can be obtained by the difference between current and previous days.
The data is available from 21 April 2020 until December 2020.

Acknowledgements
Rio de Janeiro City Hall, Municipal Health Secretariat  Others - Painel COVID-19 Rio
Raphael Fontes unanimad - Dataset Inspiration

Inspiration
Changes in the number of confirmed cases, deaths, and recoveries by neighborhood over time.
Changes in the number of confirmed cases, deaths, and recoveries at the city level.
Spread of the disease in the city.

## Contributing

This is a [read-only mirror](https://gitlab.com/data/d/openml/43509) of an [OpenML dataset](https://www.openml.org/d/43509). Contribute any changes to the dataset there. Alternatively, [fork the dataset](https://gitlab.com/data/d/openml/43509/-/forks/new) or [find an existing fork](https://gitlab.com/data/d/openml/43509/-/forks) to contribute to.

You can use [issues](https://gitlab.com/data/d/openml/43509/-/issues) to discuss the dataset and any issues.

For more information see [https://datagit.org/](https://datagit.org/).

